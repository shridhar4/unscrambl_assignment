import { Injectable } from '@angular/core';

const stops = [{
	stopId: 1,
	lat: 33.760262,
	lng: -84.384706,
	donationsRaisedInDollars: 0,
	name: 'Hertz at Portman Blvd'
},
{
	stopId: 2,
	lat: 33.760138,
	lng: -84.388043,
	donationsRaisedInDollars: 0,
	name: 'Peachtree Center Mall'
},
{
	stopId: 3,
	lat: 33.757355,
	lng: -84.386423,
	donationsRaisedInDollars: 0,
	name: 'Georgia Pacific'
},
{
	stopId: 4,
	lat: 33.758648,
	lng: -84.382754,
	donationsRaisedInDollars: 0,
	name: 'Sheraton Atlanta'
},
{
	stopId: 5,
	lat: 33.755365,
	lng: -84.384921,
	donationsRaisedInDollars: 0,
	name: 'Loudermilk Center'
},
{
	stopId: 6,
	lat: 33.756887,
	lng: -84.389417,
	donationsRaisedInDollars: 0,
	name: 'Rialto Arts Center'
},
{
	stopId: 7,
	lat: 33.759215,
	lng: -84.391719,
	donationsRaisedInDollars: 0,
	name: 'Sky View Atlanta'
},
{
	stopId: 8,
	lat: 33.762046,
	lng: -84.391708,
	donationsRaisedInDollars: 0,
	name: 'Centennial Park'
},
{
	stopId: 9,
	lat: 33.763004,
	lng: -84.387041,
	donationsRaisedInDollars: 0,
	name: 'Suntrust Plaza'
},
{
	stopId: 10,
	lat: 33.754661,
	lng: -84.380101,
	donationsRaisedInDollars: 0,
	name: 'Sweet Auburn Market'
}
];


@Injectable()

export class DonateService {

	private clone(obj) {
		let copy;

		// Handle the 3 simple types, and null or undefined
		if (null == obj || 'object' !== typeof obj) {
			return obj;
		}

		// Handle Date
		if (obj instanceof Date) {
			copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}

		// Handle Array
		if (obj instanceof Array) {
			copy = [];
			for (let i = 0, len = obj.length; i < len; i++) {
				copy[i] = this.clone(obj[i]);
			}
			return copy;
		}

		// Handle Object
		if (obj instanceof Object) {
			copy = {};
			for (const attr in obj) {
				if (obj.hasOwnProperty(attr)) { copy[attr] = this.clone(obj[attr]); }
			}
			return copy;
		}

		throw new Error('Unable to copy obj! Its type is not supported.');
	}

	private randomlyFailWith(errorMessage) {
		if ((Math.random() * 100) > 80.0) {
			throw new Error(errorMessage);
		}
	}

	getAllStops() {
		this.randomlyFailWith('Unable to read database');

		return this.clone(stops);
	}

	addDonation(stopId, donationAmountInDollars) {
		this.randomlyFailWith('Unable to connect to database');

		const stop = stops.find(function (s) {
			return s.stopId === stopId;
		});

		if (!stop) {
			throw new Error('Stop with stop id ' + stopId + ' not found.');
		}

		stop.donationsRaisedInDollars += donationAmountInDollars;
	}

}





