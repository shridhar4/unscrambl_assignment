import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DonateService } from './donat.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';


@Component({
	selector: 'app-donate',
	templateUrl: './donate.component.html',
	styleUrls: ['./donate.component.css']
})

export class DonateComponent implements OnInit {

	isLinear = true;
	donationDetailsFormGroup: FormGroup;

	// google maps zoom level
	zoom = 16;

	// initial center position for the map
	lat = 33.760138;
	lng = -84.388043;

	markers: Marker[] = [];
	selectedBusStop: Marker = null;
	donationDetails: DonataionDetails = null;


	constructor(
		private _formBuilder: FormBuilder,
		private busService: DonateService,
		private snackBar: MatSnackBar,
		private router: Router
	) { }

	/**
	* Select the bus stop
	*/
	clickedMarker(busStop: Marker) {
		console.log(`clicked the marker: ${busStop.name}`);
		this.selectedBusStop = busStop;
	}

	/**
	* Check the bus stop selected
	*/
	checkBusStop() {
		if (!this.selectedBusStop) {
			this.snackBar.open('Please Select the bus stop', 'OK', {
				duration: 3000,
			});
		}
	}


	/**
	* Set Donation Details function
	*/
	setDonationDetails() {
		if (this.donationDetailsFormGroup.valid) {
			this.donationDetails = { ...this.donationDetailsFormGroup.value };
			console.log(this.donationDetails);
		}
	}

	/**
	* Add Donation to funcdraise
	*/
	addDonation() {
		try {
			this.busService.addDonation(this.selectedBusStop.stopId, +this.donationDetails.amount);
			// this.router.navigated = false;
			this.snackBar.open(`Thank you donatating $${this.donationDetails.amount} towards ${this.selectedBusStop.name}.`, 'OK', {
				duration: 5000,
			});

			// Navigate to home
			this.router.navigate(['/home']);
		} catch (err) {
			console.error(err);
			this.snackBar.open(err, 'OK', {
				duration: 5000,
			});
		}
	}

	/**
	* Component init function
	*/
	ngOnInit() {
		try {
			this.donationDetailsFormGroup = this._formBuilder.group({
				amount: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*[1-9.][0-9]*$')])],
				name: ['', Validators.required],
				email: ['', Validators.email],
				message: ['', Validators.maxLength(100)]
			});

			this.markers = this.busService.getAllStops();
		} catch (err) {
			// console.error(err);
			this.snackBar.open(err, 'OK', {
				duration: 5000,
			});
			this.router.navigate(['/home']);
		}
		console.log(this.markers);
	}
}

// interface for type safety.
interface Marker {
	stopId: number;
	lat: number;
	lng: number;
	name: string;
	donationsRaisedInDollars?: number;
}

interface DonataionDetails {
	amount: number;
	name: string;
	email?: string;
	message?: string;
}
