import { NgModule } from '@angular/core';

import {
	MatButtonModule,
	MatToolbarModule,
	MatIconModule,
	MatCheckboxModule,
	MatInputModule,
	MatFormFieldModule,
	MatSnackBarModule,
	MatStepperModule
} from '@angular/material';

@NgModule({
	imports: [
		MatButtonModule,
		MatToolbarModule,
		MatIconModule,
		MatCheckboxModule,
		MatInputModule,
		MatFormFieldModule,
		MatSnackBarModule,
		MatStepperModule
	],
	exports: [
		MatButtonModule,
		MatToolbarModule,
		MatIconModule,
		MatCheckboxModule,
		MatInputModule,
		MatFormFieldModule,
		MatSnackBarModule,
		MatStepperModule
	]
})

export class MaterialModule {

}

